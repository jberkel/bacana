package bacana;

import javax.json.Json;
import javax.json.stream.JsonParser;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.text.Normalizer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class WikiFormatter {

    public static void main(String[] args) throws Exception {

        if (args.length < 2) {
            System.err.println(WikiFormatter.class.getSimpleName() + " <words.json> <output>");
        }

        try (FileInputStream fis = new FileInputStream(args[0])) {
            try (PrintStream out = new PrintStream(new FileOutputStream(args[1]))) {
                final JsonParser parser = Json.createParser(fis);
                print(out, parse(parser));
            }
        }
    }

    private static void print(PrintStream out, Collection<Map<String, String>> list) {
        out.println("{| class=\"wikitable sortable\"\n" +
                "|-\n" +
                "! European\n" +
                "! Brazilian\n" +
                "! Angolan");

        for (Map<String, String> obj : list) {
            String row =
                    "|-\n" +
                            "|" + format(obj.get("por")) + "\n" +
                            "|" + format(obj.get("bra")) + "\n" +
                            "|" + format(obj.get("ang"));
            out.println(row);
        }

        out.println("|}");
    }

    private static String clean(String s) {
        return s
                .replace("'", "")
                .replace("\"", "")
                .replace("[", "")
                .replace("<b>", "")
                .replace("</b>", "")
                .replace("<math>", "")
                .replace("</math>", "")
                .replace("]", "").trim();
    }

    private static String format(String value) {
        if (value != null && value.trim().length() > 0) {
            String lowerValue;
            if (value.length() > 1) {
                lowerValue = value.substring(0, 1).toLowerCase(Locale.ENGLISH) + value.substring(1);
            } else {
                lowerValue = value;
            }

            if (lowerValue.equals(value)) {
                return "{{l/pt|" + value + "}}";
            } else {
                return "{{l/pt|" + lowerValue + "|" + value + "}}";
            }
        } else {
            return "";
        }
    }


    private static Collection<Map<String, String>> parse(JsonParser parser) {
        TreeMap<String, Map<String, String>> objects = new TreeMap<>((s1, s2) -> normalize(s1).compareTo(normalize(s2)));

        Map<String, String> attrs = new HashMap<>();
        String key = null;
        while (parser.hasNext()) {
            JsonParser.Event e = parser.next();
            switch (e) {
                case KEY_NAME:
                    key = parser.getString();
                    break;
                case VALUE_STRING:
                    attrs.put(key, clean(parser.getString()));
                    break;
                case START_OBJECT:
                    attrs = new HashMap<>();
                    break;
                case END_OBJECT:
                    final String s = attrs.get("por") + attrs.values().toString();
                    objects.put(s, attrs);
                    break;
            }
        }
        return objects.values();
    }

    private static String normalize(final String title) {
        if (title == null)
            return null;

        return Normalizer.normalize(title, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "")
                .toLowerCase(Locale.US);
    }
}
