package bacana;

import org.apache.log4j.Logger;
import org.sweble.wikitext.dumpreader.DumpReader;
import org.sweble.wikitext.dumpreader.export_0_10.PageType;
import org.sweble.wikitext.dumpreader.export_0_10.RevisionType;
import org.sweble.wikitext.parser.ParserConfig;
import org.sweble.wikitext.parser.WikitextPreprocessor;
import org.sweble.wikitext.parser.nodes.WtPreproWikitextPage;
import org.sweble.wikitext.parser.utils.SimpleParserConfig;
import xtc.parser.ParseException;

import javax.json.Json;
import javax.json.stream.JsonGenerator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Parser {
    private static final Logger LOGGER = Logger.getLogger(Parser.class);
    private final ParserConfig config = new SimpleParserConfig();
    private final WikitextPreprocessor preprocessor = new WikitextPreprocessor(config);


    public static void main(String[] args) throws Throwable {
        if (args.length >= 2) {
            File input = new File(args[0]);
            System.out.println("parse " + input);

            serialize(new Parser().parseDump(input), new File(args[1]));
        } else {
            System.err.println(Parser.class.getSimpleName() + " [input]");
            System.exit(1);
        }
    }

    private static void serialize(Set<Words> words, File file) throws IOException {
        try (FileOutputStream fos = new FileOutputStream(file)) {
            try (JsonGenerator generator = Json.createGenerator(fos)) {
                generator.writeStartArray();
                for (Words word : words) {
                    word.serialize(generator);
                }
                generator.writeEnd();
            }
        }
    }

    private Set<Words> parseDump(File input) throws Exception {
        Set<Words> wordPairs = new HashSet<>();
        try (FileInputStream is = new FileInputStream(input)) {
            DumpReader dr = new DumpReader(is, Charset.forName("UTF8"), input.getName(), LOGGER, false) {
                @Override
                protected void processPage(Object mediaWiki, Object page) throws Exception {
                    PageType pageType = (PageType) page;
                    RevisionType revision = (RevisionType) pageType.getRevisionOrUpload().get(0);

                    List<Words> pairs = parse(pageType.getTitle(), revision.getText().getValue());
                    if (!pairs.isEmpty()) {
                        wordPairs.addAll(pairs);
                    }
                }
            };
            dr.unmarshal();
            dr.close();
        }
        return wordPairs;
    }

    private List<Words> parse(String title, String text) throws IOException, ParseException {
        final WtPreproWikitextPage page =
                (WtPreproWikitextPage) preprocessor.parseArticle(text, title);
        final Collector collector = new Collector();
        collector.visit(page);
        return collector.getWordList();
    }
}
