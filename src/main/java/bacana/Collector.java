package bacana;

import de.fau.cs.osr.ptk.common.AstVisitor;
import org.sweble.wikitext.parser.nodes.WtNode;
import org.sweble.wikitext.parser.nodes.WtNodeList;
import org.sweble.wikitext.parser.nodes.WtTemplate;
import org.sweble.wikitext.parser.nodes.WtTemplateArgument;
import org.sweble.wikitext.parser.nodes.WtTemplateArguments;
import org.sweble.wikitext.parser.nodes.WtText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Collector extends AstVisitor<WtNode> {
    private List<Words> wordList;

    public Collector() {
        super();
        wordList = new ArrayList<>();
    }

    @SuppressWarnings("SpellCheckingInspection")
    enum TemplateDefinition {
        /**
         * <a href="https://pt.wikipedia.org/wiki/Predefini%C3%A7%C3%A3o:PBPE">PBPE</a>
         */
        PBPE(0, 1, -1),
        /**
         * <a href="https://pt.wikipedia.org/wiki/Predefini%C3%A7%C3%A3o:PBPE2">PBPE2</a>
         */
        PBPE2(0, 1, -1),
        /**
         * <a href="https://pt.wikipedia.org/wiki/Predefini%C3%A7%C3%A3o:PBPE3">PBPE3</a>
         */
        PBPE3(0, 1, 2),
        /**
         * <a href="https://pt.wikipedia.org/wiki/Predefini%C3%A7%C3%A3o:PEPB">PEPB</a>
         */
        PEPB(1, 0, -1),
        /** <a href="https://pt.wikipedia.org/wiki/Predefini%C3%A7%C3%A3o:BRPT">BRPT</a> */
//        BRPT(1, 2, -1),
        /** <a href="https://pt.wikipedia.org/wiki/Predefini%C3%A7%C3%A3o:PTPR">PTPR</a> */
//        PTPR(2, 1, -1),
        /**
         * <a href="https://pt.wikipedia.org/wiki/Predefini%C3%A7%C3%A3o:PTA">PTA</a>
         */
        PTA(-1, -1, 0),
        /**
         * <a href="https://pt.wikipedia.org/wiki/Predefini%C3%A7%C3%A3o:PTB">PTB</a>
         */
        PTB(0, -1, -1),
        /**
         * <a href="https://pt.wikipedia.org/wiki/Predefini%C3%A7%C3%A3o:PTG">PTG</a>
         */
        PTG(-1, 0, -1),;

        TemplateDefinition(int brazilianIndex, int portugalIndex, int angolaIndex) {
            this.portugalIndex = portugalIndex;
            this.brazilianIndex = brazilianIndex;
            this.angolaIndex = angolaIndex;
        }

        private final int portugalIndex;
        private final int brazilianIndex;
        private final int angolaIndex;
    }

    private static Map<String, TemplateDefinition> TEMPLATES = new HashMap<>();

    static {
        for (TemplateDefinition t : TemplateDefinition.values()) {
            TEMPLATES.put(t.name(), t);
        }
    }

    public List<Words> getWordList() {
        return wordList;
    }

    public void visit(WtNodeList n) {
        iterate(n);
    }

    @SuppressWarnings("UnusedDeclaration")
    public void visit(WtNode n) {
    }

    @SuppressWarnings("UnusedDeclaration")
    public void visit(WtTemplate template) {

        if (!template.getName().isResolved()) {
            return;
        }

        final TemplateDefinition td = TEMPLATES.get(template.getName().getAsString());
        if (td != null) {
            String bra = extractText(template.getArgs(), td.brazilianIndex);
            String por = extractText(template.getArgs(), td.portugalIndex);
            String ang = extractText(template.getArgs(), td.angolaIndex);

            final Words words = new Words(por, bra, ang);
            System.err.println("found " + words);
            this.wordList.add(words);
        }
    }

    private String extractText(WtTemplateArguments args, int param) {
        if (param >= 0 && param < args.size()) {
            return extractText((WtTemplateArgument) args.get(param));
        } else {
            return null;
        }
    }

    private String extractText(WtTemplateArgument arg) {
        if (!arg.getValue().isEmpty() && arg.getValue().get(0) instanceof WtText) {
            return ((WtText) arg.getValue().get(0)).getContent();
        } else {
            return null;
        }
    }
}
