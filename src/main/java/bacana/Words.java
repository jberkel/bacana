package bacana;

import javax.json.stream.JsonGenerator;

public class Words {
    private final String portugal, brazil, angola;

    public Words(String european, String brazilian, String angolan) {
        this.portugal = european;
        this.brazil = brazilian;
        this.angola = angolan;
    }

    @Override
    public String toString() {
        return "Words{" +
                "portugal='" + portugal + '\'' +
                ", brazil='" + brazil + '\'' +
                ", angola='" + angola + '\'' +
                '}';
    }

    public void serialize(JsonGenerator generator) {
        generator.writeStartObject();
        if (brazil != null) generator.write("bra", brazil);
        if (portugal != null) generator.write("por", portugal);
        if (angola != null) generator.write("ang", angola);
        generator.writeEnd();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Words words = (Words) o;

        if (angola != null ? !angola.equals(words.angola) : words.angola != null) return false;
        if (brazil != null ? !brazil.equals(words.brazil) : words.brazil != null) return false;
        if (portugal != null ? !portugal.equals(words.portugal) : words.portugal != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = portugal != null ? portugal.hashCode() : 0;
        result = 31 * result + (brazil != null ? brazil.hashCode() : 0);
        result = 31 * result + (angola != null ? angola.hashCode() : 0);
        return result;
    }
}
