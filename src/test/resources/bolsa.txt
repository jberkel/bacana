[[Imagem:Denim crochet purse.jpg|thumb|right|170px|Bolsa feminina.]]
{{PBPE|Bolsa|mala}} é um acessório de [[moda]] feito de [[couro]], [[Têxtil|tecido]], [[plástico]], [[palha]], [[ráfia]], [[jeans]] ou  outros materiais, geralmente com alça (curta ou a tiracolo), usado por [[mulher]]es (e também por [[homens]]) e que serve para carregar diversos pequenos objetos, como [[dinheiro]], [[chave]]s, [[maquiagem]], [[cigarro]], [[isqueiro]], [[documento]]s, [[pente]] etc.

As bolsas menores, geralmente sem alça, e usadas em festas, são chamadas de '''carteiras''' ou '''bolsas de festa''', e podem ser mais enfeitadas, com brilho, bordadas com [[miçangas]] ou metalizadas.
==Origem==
Não existe na [[História]], referências de como seria a primeira bolsa. Mas, desde o início dos tempos, a comunicação já estava enraizada na vida humana e os povos primitivos já retratavam uma série de símbolos através das [[Pintura rupestre|pinturas rupestres]] (pinturas em rochas), e esses povos registraram através dos [[desenho]]s os seus costumes. Foram encontradas pinturas com imagens femininas com bolsa penduradas no braço. As civilizações [[Pré-história|pré-históricos]] eram [[nômade]]s, e se deslocavam de acordo com a necessidade de obter alimentos. Como já haviam descoberto que a [[Pêlo|pele]] dos animais servia para a proteção do corpo, podem ter desenvolvido também um sistema de receptáculos para carregar e proteger suas caças.
[[File:IPurse Pouch 5.JPG |thumb|right| Bolsas iPurse ]]

Uma das primeiras citações sobre bolsa feminina encontra-se na [[Bíblia]], no [[livro de Isaías]], capítulo 3:16, (palavras escritas por volta de [[750 a.C.]]) e que diz:

''Naquele dia tirará o Senhor os seus enfeites: os [[Anel|anéis]] dos [[artelho]]s, as [[touca]]s, os [[colar]]es em forma de meia-lua, os [[brinco]]s, os [[bracelete]]s, os [[vestido]]s, os [[diadema]]s, as cadeias dos artelhos, os [[cinto]]s, os [[amuleto]]s, as caixinhas de [[perfume]]s, os [[manto]]s, os [[xale]]s, as '''bolsas''', os [[espelho]]s, as capinhas de [[linho]] e as [[tiara]]s.''

O [[alforje]], predecessor das bolsas, era um saco de [[couro]] que podia ser usado na [[cintura]], nos [[ombro]]s ou na sela dos animais. Era uma a bolsa utilizada principalmente pelos homens, para carregar [[alimento]]s ou [[dinheiro]], durante a [[Idade Antiga]].
{{commons|Category:Purses}}
{{Roupas}}
[[Categoria:Acessórios de moda]]
