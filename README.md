Extract spelling and word differences in Portuguese, based on data from the Portuguese Wikipedia.

This is achieved by parsing the content and looking for templates like [PBPE][].

[PBPE]: https://pt.wikipedia.org/wiki/Predefini%C3%A7%C3%A3o:PBPE
